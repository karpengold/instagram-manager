<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
    @include('common.errors')

    <!-- New Task Form -->
        <form action="{{ url('task') }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}

        <!-- Task Name -->
            <div class="form-group">
                <label for="task" class="col-sm-3 control-label">Instaspy</label>

                <div class="col-sm-6">
                    <input type="text" name="name" id="task-name" class="form-control">
                </div>
            </div>

            <!-- Add Task Button -->
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Get profile info
                    </button>
                </div>
            </div>
        </form>
    </div>

    @if ( $tasks!= null && count($tasks) > 0 )
        <div class="panel panel-default">
            <div class="panel-heading">
                Profile info
            </div>

            <div class="panel-body">
                <table class="table table-striped task-table">


                    <thead>
                    <th>Profile</th>
                    <th>&nbsp;</th>
                    </thead>


                    <tbody>
                        @foreach ($tasks as $task)
                            <tr>

                                <td class="table-text">

                                    <div>Image id: {{ $task->image_id }}</div>
                                    <div>{{$task->comments}} comments</div>
                                    <div>{{$task->likes}} likes</div>
                                    <div>Caption: {{$task->caption}}</div>
                                    <div><a href={{$task->url}} target="_blank">image link</a> </div>

                                </td>

                            </tr>
                        @endforeach

                    </tbody>

                </table>
                <?php echo $tasks->appends(['name' => $user])->render(); ?>

            </div>
        </div>
    @endif
@endsection