<?php

namespace App\Jobs;

use App\Jobs\Job;
use GuzzleHttp\Client;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GetProfileInfo extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $userId;
    protected $beginMediaCursor;
    protected $user;
    protected $token;
    protected $url;
    protected $headers;
    protected $count;
    protected $currentCount;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($token, $url,$headers, $userId, $beginMediaCursor, $user,$count, $currentCount)
    {

        $this->token = $token;
        $this->url = $url;
        $this->headers = $headers;
        $this->userId = $userId;
        $this->beginMediaCursor = $beginMediaCursor;
        $this->user = $user;
        $this->count = $count;
        $this->currentCount = $currentCount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client([
            'headers' => [
                'x-csrftoken' => $this->token,
                'referer'     => $this->url,
                'content-type'=> 'application/x-www-form-urlencoded',
                'cookie' => $this->headers
            ],
        ]);
        $response = $client->post('https://www.instagram.com/query', [
            "form_params" => [
                "q" => "ig_user({$this->userId}) { media.after({$this->beginMediaCursor}, 20) {
                      count,
                      nodes {
                        caption,
                        code,
                        comments {
                          count
                        },
                        comments_disabled,
                        date,
                        dimensions {
                          height,
                          width
                        },
                        display_src,
                        id,
                        is_video,
                        likes {
                          count
                        },
                        owner {
                          id
                        },
                        thumbnail_src,
                        video_views
                      },
                      page_info
                    }
                    }",
                "ref" =>"users::show"
            ]
        ]);
        $media = json_decode((string)$response->getBody()) -> media -> nodes;

        $count = count($media);

        foreach($media as $current) {
            $caption ="";
            if(isset($current->caption)) {
                $caption = $current->caption;
            }
            $this->user->images()->create([
                'image_id' => $current-> id,
                'url'      => $current-> thumbnail_src,
                'likes'    => $current-> likes->count,
                'comments' => $current -> comments->count,
                'caption'  => $caption
            ]);
        }
        $beginMediaCursor = json_decode($response->getBody())->media->page_info->end_cursor;

        if($this->currentCount < $this->count){
            $job = new  GetProfileInfo($this->token, $this->url, $this->headers,$this->userId,$beginMediaCursor,$this->user, $this->count, $this->currentCount + 20);
            dispatch($job);
        }

    }
}
