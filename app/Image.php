<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected  $fillable = array('image_id', 'url', 'likes', 'comments', 'caption');
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
