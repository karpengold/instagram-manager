<?php

namespace App\Http\Controllers;


use App\Jobs\GetProfileInfo;
use App\User;
use App\Libs\Instagram;
use Illuminate\Http\Request;


class InstagramController extends Controller
{

    function connect(Request $request){
        $instagramName = $request->name;
        if(User::userExist($instagramName)){
            $user = User::getUser($instagramName);
            $tasks = $user->images()->paginate(5);
            return view('tasks', [
                'user'  => $instagramName,
                'tasks' => $tasks
            ]);
        }
        else {
            $user = User::createUser($instagramName);
        }

        $profile = new Instagram($instagramName);

        $job = new  GetProfileInfo($profile->token, $profile->url, $profile->headers,$profile->userId,$profile->beginMediaCursor,$user, $profile->mediaCount, 20);
        dispatch($job);

        return view('tasks', [
            'user' =>  $instagramName,
            'tasks' => $user->images()->paginate(5)
        ]);

    }

}