<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = array('user_name', 'user_id');

    public function images()
    {
        return $this->hasMany(Image::class);
    }
    public static function userExist($userName){
        $user = User::where('user_name', $userName)->first();
        $result = $user==null?false:true;
        return $result;
    }


    public static function createUser($instagramName){
        return User::create([
            'user_name' => $instagramName
        ]);
    }
    public static function  getUser($userName){
        return  User::where('user_name', $userName)->first();
    }
}
