<?php
namespace App\Libs;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Created by PhpStorm.
 * User: karpengold
 * Date: 15.08.16
 * Time: 15:08
 */
class Instagram
{
    public $url = "https://www.instagram.com/";
    public $userId;
    public $beginMediaCursor;
    public $mediaCount;
    public $token;
    public $headers;
    function __construct($instagramName ){
        $this->url .= $instagramName;
        $client = new Client();
        $response = $client->get($this->url);
        $crawler = new Crawler((string)$response->getBody());
        $data = $crawler->filter('script')->getNode(6)->nodeValue; //find client info
        $jsonData = json_decode(substr($data, 21 ,-1)); //get only json data, without script
        $userData = $jsonData->entry_data->ProfilePage[0]->user;

        $this->userId           = $userData->id;
        $this->beginMediaCursor = $userData->media->page_info->start_cursor;
        $this->mediaCount       = $userData->media->count;
        $this->token            = $jsonData->config->csrf_token;
        $this->headers = $response->getHeader("Set-Cookie");
    }

}